#include <iostream>
using namespace std;

int main () {
  int device_count;
  cudaGetDeviceCount(&device_count);

  cudaDeviceProp dp;

  cout << "CUDA device count: " << device_count << endl;

  for(int i = 0; i < device_count; i++) {
    cudaGetDeviceProperties(&dp, i);

    cout << i << ": " << dp.name << " with CUDA compute compatibility " << dp.major << "." << dp.minor << endl;
    cout << i << ": Clock rate = " << dp.clockRate << endl;

    cout << "MEMORY DATA" << endl;
    cout << i << ": Total memory = " << dp.totalGlobalMem / 1024 / 1024 << endl;
    cout << i << ": Total const memory = " << dp.totalConstMem << endl;
    cout << i << ": Memory pitch = " << dp.memPitch << endl;

    cout << "MULTI PROCESS" << endl;
    cout << i << ": Multi processor count = " << dp.multiProcessorCount << endl;
    cout << i << ": Shared memory per block = " << dp.sharedMemPerBlock << endl;
    cout << i << ": Registers per block = " << dp.regsPerBlock << endl;
    cout << i << ": Warp size = " << dp.warpSize << endl;
    cout << i << ": Max threads per block = " << dp.maxThreadsPerBlock << endl;
    cout << i << ": Max threads dimension of a block = " << dp.maxThreadsDim[0] << " " << dp.maxThreadsDim[1] << " " << dp.maxThreadsDim[2] << endl;
    cout << i << ": Max grid size = " << dp.maxGridSize[0] << " " << dp.maxGridSize[1] << " " << dp.maxGridSize[2] << endl;
  }

  return 0;
}